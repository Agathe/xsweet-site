---
title: "Why xSweet?"
draft: false
weight: 110
part: 1
Intro : "The most common use case for XSweet is to migrate MS Word files 'to the web'."
class: docs
---

The most common use case for XSweet is to migrate MS Word files 'to the web'. If you have any amount of content that exists in standalone MS Word files and you need to have those in some sort of web-based CMS, data store, or website, then XSweet will help you greatly.

However, XSweet is not only designed for this purpose. One of the problems of MS Word is that the underlying markup of a typical document is very messy and complex. It is hard to for machines to start with this format, as first they need to throw away a lot of superfluous 'noise' in the document markup before machine interpretation can begin. XSweet offers a very clean starting position, with the content of the original file intact _and_ a cleaner semantic structure.

As it happens, this cleaner file can also be used as an intermediary to translate to other file formats. This has, until now, typically been the role in a conversion chain played by verbose XML schemas like TEI. However, we believe a clean and faithful HTML representation of the original file is a better 'hub' format, since HTML is easier for programmers to work with (which makes improving the conversion a lot easier for most organizations) and there are far more tools out there for converting _from_ HTML to other formats than for any other file format in the world.

In short, XSweet is useful in any cases where you start with .docx and need HTML as a final format, require machine interpretation of the content, or need to end up in another format.

* XSweet provides a **faithful representation** of the contents of .docx files in HTML
* XSweet produces **clean, human-readable HTML**, making it easy to work with
* By using HTML, XSweet allows you to **take advantage of existing HTML tools** or **instantly publish to the web**
* XSweet is **modular and extensible**, allowing you to use the parts you want, skip the parts you don't, and add your own steps
* XSweet can be used with tools to **enhance content**, such as a heading inferrer and import into a WYSIWYG editor for editing, styling, and more
* XSweet is **open source, free to use, **and** always will be**
