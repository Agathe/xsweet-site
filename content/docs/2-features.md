---
title: "1.0 Features"
draft: false
weight: 120
part: 1
Intro : "XSweet 1.0 conversion addresses the following"
class: docs
---

XSweet 1.0 conversion addresses the following:

* produces HTML from `.docx` files
* basic list extraction, including nested lists
* tables
* inline and paragraph-level styling
    * font information: size and font family
    * bold, italic, underline
    * superscript and subscript
    * small-caps
    * indentation and spacing
    * text alignment: left, right, center
    * applied MS Word Styles
* header interpolation by multiple approaches
    * based on visual formatting
    * based on outline-level
    * based on custom configurations
* hyperlink extraction and plain-text url linking
* alternative outputs, including plain text and document analysis
* end notes and foot notes as linked callouts
* special characters

Images and Math are currently not handled, and are slated for future releases to be prioritized depending on need expressed by the community. See the [XSweet roadmap](https://gitlab.coko.foundation/XSweet/XSweet "gitlab.coko.foundation/XSweet/XSweet") for other future developments.