---
title: "Further Reading"
draft: false
weight: 150
part: 1
Intro : "Although we see HTML as our primary use case, XSweet also proposes"
class: docs
---
Although we see HTML as our primary use case, XSweet also proposes a new approach to what has previously been known as a 'hub format'. So, here is a collection of some writings on this approach, most written by the XSweet team, with a few other contributions.
[Text. You keep using that word…](https://www.balisage.net/Proceedings/vol19/html/Sperberg-McQueen02/BalisageVol19-Sperberg-McQueen02.html "www.balisage.net") by C. M. Sperberg-McQueen, (Co-editor of XML Recommendation (1998), long-time Editor of TEI)

[A Typescript for the web](https://coko.foundation/typescript-for-the-web/ "coko.foundation/typescript-for-the-web") by Wendell Piez and Adam Hyde

[Uphill to XML with XSLT, XProc … and HTML](http://www.balisage.net/Proceedings/vol20/html/Piez02/BalisageVol20-Piez02.html "www.balisage.net") by Wendell Piez

[HTML First?: Testing an alternative approach to producing JATS from arbitrary (unconstrained or "wild") .docx (WordML) format](https://www.ncbi.nlm.nih.gov/books/n/jatscon17/piez/ "www.ncbi.nlm.nih.gov/books") by Wendell Piez

[Why HTML Typescript is a good idea](https://www.adamhyde.net/why-html-typescript-is-a-good-idea/ "www.adamhyde.net/why-html-typescript-is-a-good-idea") by Adam Hyde 

[HTML Typescript – redistributing labor](https://www.adamhyde.net/typescript-redistributing-labor/ "https://www.adamhyde.net/typescript-redistributing-labor") by Adam Hyde
