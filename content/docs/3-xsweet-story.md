---
title: "The XSweet Story"
draft: false
weight: 130
part: 1
Intro : "Microsoft Word has long been the de facto standard"
class: docs
---
Microsoft Word has long been the de facto standard for authoring and editing manuscripts for publication. But while they (sometimes) look nice to a human reader, a peek under the hood reveals a messy slurry of largely unstructured text, tags, and cruft. As a result, publishing processes tend to look something like this:

1. Authors submit one or many .docx files
2. Editors, copy editors, reviewers, and authors make revisions, passing different versions of the static files back and forth, until the content is finalized
3. Finally, production staff cajole the content into a publishable format, which can be time-intensive, costly, and error-prone

The first two steps are slow, since it is a process of emailing MS Word files to many parties, or storing them in a common data store. The last step is both slow _and expensive_ as it requires an external vendor to wrangle the content out of the tortured MS Word file into the (multiple) target output formats.

But what if content could be converted to a well-structured digital format at the beginning of the process and maintained throughout the entire workflow? The last step could then be easily automated.

Adam Hyde, Coko Foundation cofounder and longtime web-based publishing platform creator, posited that HTML was the perfect format for this: robust enough to reflect the required formatting and document structure markup, supported by a wide range of existing HTML tools, able to have structure added over time, and by nature easy to instantly publish. The idea for XSweet was born.

Word documents are essentially - and ironically - poorly structured XML documents, and it was a challenge to find a good XML programmer willing to try to impose order on such a chaotic input. After all, you can't go from unstructured to structured data, right? Enter Wendell Piez, a seasoned XML expert who wasn't afraid of ambiguity. Accepting that author inputs aren't nicely structured, it could still be possible extract content and formatting from the XML into HTML and CSS, and then given the proper tools, a human could take it the last mile and end up with a nicely structured HTML document.

Since then, Wendell and Alex Theg have developed XSweet for use with many Coko products including [Ketida](https://ketida.community/ "ketida.community") (previously named Editoria), an open source, in-browser book production platform, and xpub - a journal platform. Ketida and xpub both use XSweet to ingest author Word files as HTML that can be edited and styled in a WYSIWYG editor, then instantly exported in a variety of formats. Today, XSweet has grown into a powerful tool that can support virtually any web-based publishing platform. We are excited to continue development and see how it is used.