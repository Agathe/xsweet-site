---
title: "Credits"
draft: false
weight: 160
part: 1
Intro : "XSweet is a project from Cabbage Tree Labs"
class: docs
---
XSweet is a project from [Cabbage Tree Labs](https://cabbagetreelabs.org "cabbagetreelabs.org"), conceived by Adam Hyde and co-designed with veteran XML programmer [Wendell Piez](http://wendellpiez.com/ "wendellpiez.com"). XSweet scripts were built and tested by Wendell Piez and Alex Theg.

License : [MIT Open Source License](https://en.wikipedia.org/wiki/MIT_License "en.wikipedia.org/wiki/MIT_License")