---
title: "Related Projects"
draft: false
weight: 140
part: 1
Intro : "As with all software projects XSweet owes a debt of gratitude"
class: docs
---
As with all software projects XSweet owes a debt of gratitude to those that have come before and those that still exist in this space. If XSweet does not meet your needs we recommend you also look at the following for docx to HTML conversion (only open source projects listed):

[pandoc](http://pandoc.org/ "pandoc.org") - an all purpose command line utility for conversions.

[OxGarage](http://www.tei-c.org/oxgarage/ "www.tei-c.org/oxgarage") - uses TEI as an intermediary 'hub' format. We would particularly like to honor the work of Sebastian Rhatz who met with us many times to discuss various conversion strategies and who passed away in 2016. Sebastian was a very kind and generous man.

[soffice](https://www.libreoffice.org/ "www.libreoffice.org") - the LibreOffice 'headless' binaries.

[ebook convert](https://manual.calibre-ebook.com/generated/en/ebook-convert.html "manual.calibre-ebook.com") - the handy standalone python script supplied by the Calibre ebook reader.

[Python-ooxml](https://github.com/booktype/python-ooxml "github.com/booktype/python-ooxml") - python library for conversion, part of the Booktype project.

[meTypeSet](https://github.com/MartinPaulEve/meTypeset "github.com/MartinPaulEve/meTypeset") - project for .docx to JATS conversion by Coko friend Martin Eve.

[Mammouth](https://github.com/mwilliamson/python-mammoth "github.com/mwilliamson/python-mammoth") - python scripts for docx to HTML conversion, intended mostly for use with docx documents that have used named styles. -

[transpect](https://transpect.github.io/ "transpect.github.io") - a suite of data conversion and validation tools