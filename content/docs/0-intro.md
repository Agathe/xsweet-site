---
title: "Introduction"
draft: false
weight: 100
part: 1
Intro : "XSweet is a free, open source conversion tool for converting Microsoft Word documents (.docx) into HTML and beyond"
class: docs
---

XSweet is a free, open source conversion tool for converting Microsoft Word documents (.docx) into HTML and beyond.

Built as a series of XSL (eXtensible Stylesheet Language) transformation steps, it's designed to be modular and flexible. Use it out of the box, or modify and extend it to meet your needs.

XSweet is designed with publishing in mind, but can be used for any application. XSweet extracts the contents of MS Word documents from their underlying XML into HTML, which can be published as-is, imported into an application, or used as a _lingua franca_ to convert it into another format altogether. Beyond simple conversion to HTML, XSweet also includes a number of optional enhancement steps, including:

* Transformations to load HTML files into a specific application with it's own document semantics (for example we have done this for, [Wax](https:waxjs.net), the Coko Foundation's web-based word processor for styling, editing and improving content)
* Recognizing plain-text URLs and converting them to hyperlinks
* Inferring and tagging headings and heading levels based on visual formatting
and more.
* a set of 'macros' for microtypesetting transformations (for example, translating '...' into a real elipsis, transforming double spaces to single spaces etc)

XSweet is a project from the [Cabbage Tree Labs](https://www.cabbagetreelabs.org/ "www.cabbagetreelabs.org"), conceived by [Adam Hyde](https://www.adamhyde.net/ "www.adamhyde.net") and co-designed with veteran XML programmer [Wendell Piez](http://wendellpiez.com/ "wendellpiez.com"). XSweet scripts were built and tested by Wendell Piez and Alex Theg. If you'd like to talk to anyone for help working with XSweet you can find us on the [Coko Mattermost chat](https://mattermost.coko.foundation/coko/channels/xsweet/ "mattermost.coko.foundation/coko/channels/xsweet").

You can find the code [here](https://gitlab.coko.foundation/XSweet "gitlab.coko.foundation/XSweet"). Please see the documentation and using sections to understand how to work with XSweet.
