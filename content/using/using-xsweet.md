---
title: "Using XSweet"
draft: false
weight: 300
part: 1
Intro : "There are a variety of ways to run XSweet, HTMLevator, and Editoria Typescript"
class: using
---
## XSweet with *nix and beyond
One of the powerful features of XSweet is that it can be run in a wide variety of environments. For example, XSWeet can be integrated into an application [see Ketida and Kotahi from Coko for examples](https://coko.foundation). XSweet can also be run using a Unix-like terminal and a few scripts; indeed, this was how much of the testing was done.

You can find some [example Bash and Ruby scripts here](https://gitlab.coko.foundation/XSweet/XSweet_runner_scripts/ "gitlab.coko.foundation/XSweet/XSweet_runner_scripts") (not maintained or warrantied). These are used for XSweet's' testing and development to quickly convert many files and inspect each step's inputs and outputs.

{{< figure src="../images/xsweet_bash-768x291.png" >}}

Be aware that the above scripts aren't recommended for production; they're simply offered as an illustration of what is possible. XSweet can also be run as a very speedy PHP service, in an XML IDE, using XPL pipelines, and more. For Ketida and Kotahi (mentioned above) the team ran XSweet as a separate containerized microservice.
