---
title: "How to Get Involved"
draft: false
weight: 400
part: 1
Intro : "Editoria Typescript transforms HTML into a format required for the Coko Foundation's"
class: how-to
---

## Contributing

We would like it very much if you could help us improve XSweet. There are essentially two ways to do this.

1. XSLT Pros - you can help by tracking the <a href="">[Issues in the XSweet GitLab](https://gitlab.coko.foundation/XSweet/XSweet/issues "gitlab.coko.foundation/XSweet/XSweet/issues") and either:
    (A) participating in the discussions on a per Issue basis, or
    (B) cloning the repo and fixing some of the Issues that come up, then making a merge request for Wendell and Alex to review
2. QA Folk - if you run files through XSweet and discover issues, please log these as an Issue <a href="">[in GitLab](https://gitlab.coko.foundation/XSweet/XSweet/issues "gitlab.coko.foundation/XSweet/XSweet/issues").


## Specific Asks

At this moment we would love help with these specific items:
* QA - run your test corpus of docs through (even if small) and *carefully* document what fails. Write it as a Issue in Gitlab describing the issue, include XML and HTML snippets for clarification, and (if possible) include the test document.
* Dev - conversion of Math. This is a complex issue comprising of many constituent parts including (but not exclusively):
    * conversion of MS Word non-standard MathML to standardised MathML
    * conversion of MathType to MathML
* Dev - writing unit tests

## Contact us

The main avenues for communication are via the Issues, but also in the <a href="">[Coko mattermost XSweet chat channel](https://mattermost.coko.foundation/coko/channels/xsweet "mattermost.coko.foundation/coko/channels/xsweet").

You can also contact [Adam by email](mailto:adam@coko.foundation).
