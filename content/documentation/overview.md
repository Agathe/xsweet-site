---
title: "Overview"
draft: false
weight: 200
part: 1
Intro : "XSweet is divided into three separate repositories"
class: documentation
---
XSweet is divided into three separate repositories, which are grouped by their primary concerns:

## XSweet Core
XSweet Core is designed to extract data from MS Word, clean it up, and produce a good representation of the contents as HTML.

Word XML:
{{< figure src="../images/word_xml-768x1504.png" >}}

Extracted HTML:
{{< figure src="../images/html-768x251.png" >}}


[Documentation]({{< ref "xsweet-core.md" >}} "xsweet-core.md")

[Repository](https://gitlab.coko.foundation/XSweet/XSweet "gitlab.coko.foundation/XSweet/XSweet")

## HTMLevator
HTMLevator contains optional enhancements for the HTML, above and beyond simple extraction. This includes features such as plain text URL recognition and linking, heading inferring, copyediting cleanups, and more.

[Documentation]({{< ref "HTMLevator.md" >}} "HTMLevator.md")

[Repository](https://gitlab.coko.foundation/XSweet/HTMLevator "gitlab.coko.foundation/XSweet/HTMLevator")

## Editoria Typescript
Editoria Typescript transforms HTML to be loaded into the [Wax](https://gitlab.coko.foundation/wax/wax "/gitlab.coko.foundation/wax") WYSIWYG word processor for [Ketida](https://ketida.community/ "ketida.community") (previously named Editoria), where it can be styled, revised, and collaborated upon. This is a use-case-specific transformation chain, and a demonstration of how the HTML produced by XSweet Core and HTMLevator can be used as a pass-through format for conversions. Similar conversion chains can target other specific use cases in the same way.

[Documentation]({{< ref "editoria-typescript.md" >}} "editoria-typescript.md")

[Repository](https://gitlab.coko.foundation/XSweet/editoria_typescript "gitlab.coko.foundation/XSweet/editoria_typescript")

## XSLT tools
### Saxon
XSweet ships with [Saxon HE 9.8](https://www.saxonica.com/documentation/documentation.xml "www.saxonica.com/documentation"), which can be used to run the XSweet pipeline. Most of the testing has been done with this version of Saxon. For example syntax for usage from the command line and syntax, see its invocation in [this script](https://gitlab.coko.foundation/XSweet/XSweet_runner_scripts/blob/master/execute_chain.sh#L56 "gitlab.coko.foundation/XSweet").

### XSLT versions
XSweet is built using XSLT v2.0 and XSLT v3.0 stylesheets. Saxon HE 9.8 is an XSLT 3.0 processor. You may see warning messages that you are `Running an XSLT 2.0 stylesheet with an XSLT 3.0 processor`. This has not caused any issues in testing and development, but be aware of this if you add your own XSLT sheets and use features specific to one version or the other.

### A Note on .xpl files
An `.xpl` file is an XML document instance using the XProc pipelining language. XProc is a W3C Recommendation that describes the definition and arrangement of XML transformation and modification operations as **pipelines**, typically sequences or series of operations with defined inputs and outputs ("ports"). Inasmuch as XSweet's architectural model is exactly such a pipeline of transformations, this becomes a utilitarian way for us to stand up processes for development and testing. An XProc file describes a chain of processes along with the resources required along the way (such as stylesheets or configurations). Run an XProc pipeline using an XProc processor such as XML Calabash, or using an XML IDE with XProc support. In addition to development and testing, we believe XProc (albeit not exactly these pipelines) could potentially be useful in some deployments. However, XProc is only one of multitudinous ways of orchestrating pipelines -- and we have INK, so we don't need, or use, any of these files to run XSweet in production.

Feel free to use the `.xpl` files included in XSweet's repositories but be aware they are for testing/development purposes and are not supported or externally documented.